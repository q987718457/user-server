package com.heima.daomain;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

    @Data
    @TableName("t_user")
    public class User {

        @TableId
        private Long id;
        private String name;
        private Integer age;
    }

