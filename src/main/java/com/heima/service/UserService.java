package com.heima.service;

import com.heima.daomain.User;

public interface UserService {
    User findById(Long id);
}
