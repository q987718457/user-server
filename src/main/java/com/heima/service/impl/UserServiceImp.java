package com.heima.service.impl;

import com.heima.daomain.User;
import com.heima.mapper.UserMapper;
import com.heima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override

    public User findById(Long id) {



        return userMapper.selectById(id);
    }
}
