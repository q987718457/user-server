package com.heima.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.heima.daomain.User;

public interface UserMapper  extends BaseMapper<User> {
}
